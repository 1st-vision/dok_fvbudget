.. |label| replace:: Budgetgrenzen für Nutzer
.. |snippet| replace:: FvBudget
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.5.6
.. |version| replace:: 1.0.0
.. |php| replace:: 7.2


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Plugin ermöglicht es Ihnen, Kunden ein Budget für einen bestimmbaren Zeitraum vorzugeben.

Frontend
--------
Im Kundenaccount sowie im letzten Checkout-Schritt wird eine Übersicht der aktuellen Budgetdaten des Kunden angezeigt.
Ist das Budget ausreichend, so kann bestellt werden und der Nettobetrag der Bestellung wird vom Budget des Kunden abgezogen.
Ist das Budget nicht ausreichend, so wird der Bestellbutton ausgeblendet und oberhalb sowie anstelle des Bestellbuttons eine Warnung angezeigt, dass das Budget unzureichend ist.

.. image:: FvBudgetFrontendAccount.png
.. image:: FvBudgetFrontendCheckoutOk.png
.. image:: FvBudgetFrontendCheckoutBlocked.png

Backend
-------
Konfiguration
_____________

.. image:: FvBudgetConfig.png

Hier können für neue Nutzer Standardwerte des Budgets angegeben werden, die geladen werden wenn kein anderer Wert hinterlegt ist.
Soll standardmäßig keine Bestellung möglich sein, reicht es die Budgetsumme auf 0 zu setzen.
Soll standardmäßig keine Budgetgrenze gegeben sein, so kann man den Wert auf -1 setzen, wodurch der Kunde vom Plugin nicht eingeschränkt wird.

Kundenbudget anpassen
__________________________

 - **Budget Beginn Intervall** Datum an dem das aktuelle Budgetintervall begonnen hat
 - **Restbudget netto** verbleibendes Nettobudget für dieses Intervall
 - **Budgetsumme netto** Nettobudget auf das nach Intervallende zurückgesetzt wird. Wird hier -1 angegeben, hat der Kunde ein unbegrenztes Budget.
 - **Budget Typ** gibt an welche Zeiteinheit für den Rücksetzen des Budgets einscheident ist. (Tag, Woche, Monat, Jahr)
 - **Budget Typ Wert** gibt an wie viele Zeiteinheiten vor den Rücksetzen des Budgets verstreichen müssen. (Beispiel: alle 4 Wochen)

.. image:: FvBudgetBackend.png

Textbausteine
_____________

**frontend/checkout/confirm**
 - ConfirmErrorInsufficientBudgets

**frontend/fvbudget/overview**
 - AccountBudgetTypeDay
 - AccountBudgetTypeWeek
 - AccountBudgetTypeMonth
 - AccountBudgetTypeYear
 - AccountHeaderBudget
 - budgetsumBudget
 - budgetRemainingValueBudget
 - budgetstartValueBudget
 - budgetendValueBudget

Technische Beschreibung
------------------------

Die Hinterlegung der Budgetdaten erfolgt in den angelegten Freitextfeldern - das Ende des Intervalls wird jedoch immer live berechnet, wofür die Methode FvBudget::calcBudgetEnd($startDate, $type, $typeValue) nutzbar ist.
Nach Ende des Intervalls wird das Budget über einen Cronjob zurückgesetzt - diesen findet man im CronSubscriber.
Alle Budgetdaten inkl. dem Budgetende findet man auch in der Smartyvariable **fvBudgetData** welche auf überall im Frontend hinzugefügt wird,
wodurch man das Plugin leicht um zusätzliche Ansichten erweitern kann.

Muss man diese seperat holen und sind sie nicht über den View verfügbar, sollte man die Funktion FvBudget::getBudgetData($userId) nutzen,
da diese die Standardwerte aus der Konfiguration holt, falls kein Budget gesetzt wurde.

Zum Nutzen der Methoden calcBudgetEnd und getBudgetData kann man eine Plugininstanz schnell über die statische Methode FvBudget::getPlugin() erhalten.

Der FrontendSubscriber behandelt alle frontendbezogenen Funktionen - dazu gehört die Einbindung der Template-Dateien und das verfügbar machen der fvBudgetData-Variable.
Außerdem wird hier das ausblenden des Bestellbuttons und anzeigen der Warnung verarbeitet - sowie, im Falle einer Umgehung das unterbinden einer Bestellung ohne nötiges Budget direkt per hook auf die Checkout::finishAction.


Freitextfelder
-----------------------------

 - s_user_attributes.fv_budgetsumme
 - s_user_attributes.fv_budgettyp
 - s_user_attributes.fv_budgettypwert
 - s_user_attributes.fv_budgetbegin
 - s_user_attributes.fv_budgetrest

Modifizierte Template-Dateien
-----------------------------

 - frontend/checkout/confirm.tpl
 - frontend/account/index.tpl
